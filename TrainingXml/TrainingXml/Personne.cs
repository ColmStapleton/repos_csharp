﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrainingXml
{
    [XmlRoot("Individu")]
    [XmlInclude(typeof(Employee))]
    public class Personne
    {
        [XmlElement("Prénom")]
        public string FirstName { get; set; } //prop
        [XmlElement("Nom")]
        public string LastName { get; set; }
        //[XmlAttribute("IdentifiantUnique")]
        [XmlIgnore]
        public int Id { get; set; }

        [XmlArray("Adresses")]
        [XmlArrayItem("Adresse")]
        public List<Adresse> MesAdresses {get;set; }

        public Personne(string FirstName, string LastName, List<Adresse> adresses, int Id)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MesAdresses = adresses;
            this.Id = Id;
        }

        public Personne(string FirstName, string LastName, int Id)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Id = Id;
        }

        public Personne()
        {

        }
    }
}

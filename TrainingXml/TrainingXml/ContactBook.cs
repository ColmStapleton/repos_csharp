﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrainingXml
{
    [XmlRoot("CarnetAdresses")]
    public class ContactBook
    {
        public Personne Owner;
        public List<Personne> ListeContacts { get; set; }

        public ContactBook()
        {
            ListeContacts = new List<Personne>();
        }

        public ContactBook(List<Personne> list)
        {
            this.ListeContacts = list;
        }

        public void AddPersonne(Personne personne)
        {
            ListeContacts.Add(personne);
        }
    }
}

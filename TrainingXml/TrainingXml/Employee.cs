﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingXml
{
    public class Employee : Personne
    {
        public string Company { get; set; }
        public string Position { get; set; }
        public double Salary { get; set; }

        public Employee()
        {
            this.Company = Company;
            this.Position = Position;
            this.Salary = Salary;
        }

        public Employee(string FirstName, string LastName, int Id, string Company, string Position, double Salary) : base(FirstName, LastName, Id)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Id = Id;
            this.Company = Company;
            this.Position = Position;
            this.Salary = Salary;
        }

        public Employee(string FirstName, string LastName, List<Adresse> adresses, int Id, string Company, string Position, double Salary) : base(FirstName, LastName, adresses, Id)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MesAdresses = adresses;
            this.Id = Id;
            this.Company = Company;
            this.Position = Position;
            this.Salary = Salary;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrainingXml
{
    class Program
    {
        static void Main(string[] args)
        {
            Adresse a1 = new Adresse("Avenue de la Foret Noire",7,"Strasbourg","67000","France");
            Adresse a2 = new Adresse("Impasse Quinta Florentina", 16, "Strasbourg", "67200", "France");
            Adresse a3 = new Adresse("Rue des Pigeons", 10, "Strasbourg", "67100", "France");
            Adresse a4 = new Adresse("Rue Principale", 7, "Schiltigheim", "67300", "France");
            List<Adresse> adresses = new List<Adresse>();
            adresses.Add(a1);
            adresses.Add(a2);

            List<Adresse> adresses2 = new List<Adresse>();
            adresses2.Add(a3);
            adresses2.Add(a4);

            Personne p1 = new Personne("Paul","Schmit",adresses,1);
            p1.LastName = "Schmitt";

            Personne p2 = new Personne("Jean", "Schmitt",adresses2,2);
            Personne p3 = new Personne("Bibi", "Super", adresses, 3);

            //Console.WriteLine(p1.FirstName+" "+p1.LastName);
            //Console.WriteLine(p2.FirstName+" "+p2.LastName);

            List<Personne> personnes = new List<Personne>();
            personnes.Add(p1);
            personnes.Add(p2);

            ContactBook contacts = new ContactBook(personnes);
            XmlSerializer xs = new XmlSerializer(typeof(ContactBook));

            contacts.AddPersonne(p3);
            Employee employe = new Employee("Satya", "Nadella",4,"Microsoft", "PDG", 1000000);
            contacts.Owner = employe;
            //contacts.AddPersonne(employe);

            using (StreamWriter wr = new StreamWriter("personnesAvecAdresse.xml"))
            {
                xs.Serialize(wr, contacts);
            }

            /*
            XmlSerializer xsPerson = new XmlSerializer(typeof(Personne));
            using (StreamReader rd = new StreamReader("personneSansAdresse.xml"))
            {
                Personne p = xsPerson.Deserialize(rd) as Personne;
                Console.WriteLine("Id : {0}", p.Id);
                Console.WriteLine("Nom : {0}, {1}", p.FirstName, p.LastName);
            }
            */

            XmlSerializer xsListePersonnes = new XmlSerializer(typeof(ContactBook));
            using (StreamReader rd = new StreamReader("personnesAvecAdresse.xml"))
            {
                ContactBook cb = xsListePersonnes.Deserialize(rd) as ContactBook;
                foreach (Personne p in cb.ListeContacts)
                {
                    Console.WriteLine("Id : {0}", p.Id);
                    Console.WriteLine("Nom : {0}, {1}", p.FirstName, p.LastName);
                    foreach (Adresse a in p.MesAdresses)
                    {
                        Console.WriteLine("Adresse : {0},{1},{2} {3},{4}", a.NumDeRue, a.NomDeRue, a.Ville, a.CodePostal, a.Pays);
                    }
                }
            }



            Console.ReadLine();

        }
    }
}

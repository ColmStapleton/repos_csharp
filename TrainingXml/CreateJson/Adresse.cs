﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CreateJson
{
    public class Adresse
    {
        public string NomDeRue { get; set; }
        public int NumDeRue { get; set; }
        public string Ville { get; set; }

        public string CodePostal { get; set; }
        public string Pays { get; set; }

        public Adresse()
        {

        }

        public Adresse(string nomRue, int numRue, string ville, string cp, string pays)
        {
            this.NomDeRue = nomRue;
            this.NumDeRue = numRue;
            this.Ville = ville;
            this.CodePostal = cp;
            this.Pays = pays;
        }

    }
}

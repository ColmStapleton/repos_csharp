﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CreateJson
{
    public class Personne
    {
        public string FirstName { get; set; } //prop
        public string LastName { get; set; }
        public int Id { get; set; }

        public List<Adresse> MesAdresses {get;set; }
        public Adresse Adresse { get; set; }

        public Personne(string FirstName, string LastName, List<Adresse> adresses, int Id)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MesAdresses = adresses;
            this.Id = Id;
        }

        public Personne(string FirstName, string LastName, int Id)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Id = Id;
        }

        public Personne(string FirstName, string LastName, Adresse adresse, int Id)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Adresse = adresse;
            this.Id = Id;
        }

        public Personne()
        {

        }
    }
}

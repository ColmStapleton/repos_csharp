﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateJson
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee employee = new Employee()
            {
                Id = 1,
                Company = "Alphabet",
                FirstName = "John",
                LastName = "Doe",
                Salary = 10000,
                Position = "Accounting",
                Adresse = new Adresse()
                {
                    NomDeRue = "Rue des Frères",
                    NumDeRue = 12,
                    CodePostal = "67000",
                    Ville = "Strasbourg",
                    Pays = "France"
                }
            };

            string jsonResult = JsonConvert.SerializeObject(employee);
            string path = @"C:\Users\utilisateur\source\repos\TrainingXml\CreateJson\bin\employee.json";

            using (StreamWriter t = new StreamWriter(path, true))
            {
                t.WriteLine(jsonResult.ToString());
                t.Close();
            }

            Console.ReadLine();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AffichageX
{
    
    class Program
    {
        public static int size;
        static void Main(string[] args)
        {

            Console.WriteLine("Veuillez saisir le caractère à afficher.");

            String caractere = Console.ReadLine();
            Boolean validChar = false;
            while (!validChar)
            {
                if (caractere.Length != 1)
                {
                    Console.WriteLine("Veuillez saisir UN caractère à affichier.");
                    caractere = Console.ReadLine();
                }
                else if (caractere.All(char.IsDigit))
                {
                    Console.WriteLine("Veuillez saisir un caractère et pas un nombre");
                    caractere = Console.ReadLine();
                }
                else
                {
                    validChar = true;
                }
            }

            Boolean validNum = false;
            Console.WriteLine("Veuillez saisir la taille (entre 1 et 10) du X à afficher");
            int espacement = 0;

            while (!validNum)
            {
                try
                {
                    espacement = int.Parse(Console.ReadLine());
                    if (espacement >= 1 && espacement <= 10)
                    {
                        validNum = true;
                    }
                    else
                    {
                        Console.WriteLine("Veuillez saisir un nombre entre 1 et 10.");
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Veuillez saisir un nombre entre 1 et 10 et pas une lettre.");
                }
            }
            Console.WriteLine();


            size = espacement;
            afficherX();

            Console.ReadLine();

            /*
            int taille = espacement*10;
            
            string[,] liste = new string[taille,taille];
            int countI = 0;

            for (int i = 0; i < taille; i++)
            {
                for (int j = 0; j < taille; j++)
                {
                    if ((j <= espacement || j >= taille - espacement) && (j >= countI || j <= taille - countI) && liste[j, i] != " ")
                    {
                        liste[i, j] = "X";
                        Console.WriteLine(i + ";" + j + " ; espacement ="+espacement+" ; count = "+countI);
                    }
                    else
                    {
                        liste[i, j] = " ";
                        liste[j, i] = " ";
                    }
                }
                espacement++;
                countI++;
            }


            for (int i = 0; i < taille; i++)
            {
                for (int j = 0; j < taille; j++)
                {
                    Console.Write(liste[i, j]);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
            */
        }
        static void afficherLigne(int nbLigne)
        {
            for (int j = 0; j <= nbLigne; j++)
                Console.Write(' ');
            for (int j = size; j > 0; j--)
                Console.Write('X');
            for (int j = 0; j < ((size - nbLigne) *2); j++)
                Console.Write(' ');
            for (int j = 0; j < size; j++)
                Console.Write('X');
            Console.WriteLine();
        }
        static void afficherX()
        {
            for (int nbLigne = 0; nbLigne <= size; nbLigne++)
                afficherLigne(nbLigne);
            for (int nbLigne = size; nbLigne >= 0; nbLigne--)
                afficherLigne(nbLigne);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriQuentin
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 0, 3, 5, 11, 2, -2, 1, 4, 6, -3, 9, 7, 8, 10, -1 };
            array = SortDesc(array);
            View(array);
            Console.ReadLine();
        }
        static int[] SortDesc(int[] array)
        {
            for (int i = array.Length - 1; i >= 1; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        int temp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = temp;
                    }
                }
            }
            return array;
        }
        static void View(int[] array)
        {
            foreach (int i in array)
            {
                Console.Write(i + " ");
            }
        }
    }
}

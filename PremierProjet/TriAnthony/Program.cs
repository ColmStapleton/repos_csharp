﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriAnthony
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tabDeTri = { 0, 3, 5, 11, 2, -2, 1, 4, 6, -3, 9, 7, 8, 10, -1 };
            foreach (int elt in tabDeTri)
            {
                Console.Write(elt + " ");
            }
            Console.WriteLine();

            //initialisation des variables de tri et d'échange
            int indexTri = 0;
            int temp = 0;

            //s'il faut échanger le premier index
            if (tabDeTri[0] < tabDeTri[1])
            {
                //variable temporaire
                temp = tabDeTri[0];

                //tri des données
                tabDeTri[0] = tabDeTri[1];
                tabDeTri[1] = temp;
            }

            //tri du tableau (commence par 1 pour ne pas sortir du tableau lors de la vérification)
            for (int i = 1; i < tabDeTri.Length; i++)
            {
                //enregistrement de la variable d'index vérifié
                indexTri = i;

                //tant que la valeur actuelle n'est pas triée
                while (tabDeTri[indexTri - 1] < tabDeTri[indexTri] && indexTri > 1)
                {
                    //variable temporaire
                    temp = tabDeTri[indexTri - 1];

                    //tri des données
                    tabDeTri[indexTri - 1] = tabDeTri[indexTri];
                    tabDeTri[indexTri] = temp;

                    //index précédent
                    indexTri--;
                }

                //s'il ne reste plus que le premier index à échanger
                if (indexTri == 1 && tabDeTri[indexTri - 1] < tabDeTri[indexTri])
                {
                    //variable temporaire
                    temp = tabDeTri[indexTri - 1];

                    //tri des données
                    tabDeTri[indexTri - 1] = tabDeTri[indexTri];
                    tabDeTri[indexTri] = temp;
                }
            }

            foreach (int elt in tabDeTri)
            {
                Console.Write(elt+" ");
            }
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}

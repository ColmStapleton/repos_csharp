﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeuxiemeProjet
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            int[,] test = new int[3,4];
            int count = 0;

            /*
            Console.WriteLine(test.GetLength(0));
            for (int i = 0; i < test.GetLength(0); i++)
            {
                for (int k = 0; k < test.GetLength(1); k++)
                { 
                    test[i, k] = count;
                    Console.WriteLine("i="+i+" k="+k+" res="+test[i, k]);
                    count++;
                }
            }

            test[0, 0] = 15;
            test[0, 1] = 1;
            test[0, 2] = 50;
            test[0, 3] = 50;
            test[1, 0] = 4;
            test[1, 1] = 20;
            test[1, 2] = 6;
            test[1, 3] = 7;
            test[2, 0] = 8;
            test[2, 1] = 9;
            test[2, 2] = 5;
            test[2, 3] = 2;

            /*
            count = 0;           
            foreach (int elt in test)
            {
                Console.Write(elt+" ");
                count++;
                if (count%4 == 0)
                {
                    Console.WriteLine();
                }
            }

            //int[,] newTab = new int[3,4];
            int[] tempTab = new int[test.Length];

            for (count = 0; count < test.Length; count++)
            {
                int max = 0;
                int maxI = 0;
                int maxJ = 0;
                for (int i = 0; i < test.GetLength(0); i++)
                {
                    for (int j = 0; j < test.GetLength(1); j++)
                    {
                        if (test[i, j] > max)
                        {
                            max = test[i, j];
                            tempTab[count] = max;
                            maxI = i;
                            maxJ = j;
                        }
                    }
                }
                test[maxI, maxJ] = 0;
            }

            for (int i = 0; i < tempTab.Length; i++)
            {
                Console.Write(tempTab[i]+ " ");
            }

            /*
            foreach (int elt in tempTab)
            {
                for (int i = 0; i < newTab.GetLength(0); i++)
                {
                    for (int j = 0; j < newTab.GetLength(1); j++)
                    {
                        newTab[i, j] = elt;
                        Console.WriteLine(elt);
                    }
                }
            }

            /*
            foreach (int elt in newTab)
            {
                Console.Write(elt + " ");
                count++;
                if (count % 4 == 0)
                {
                    Console.WriteLine();
                }
            }


            /*
            count = 0;
            while(count<7)
            {
                Console.WriteLine("Je suis en formation");
                count++;
            }
            //Console.WriteLine(test[1, 1]);
           
            */


            /*
            Personne p = new Personne("Stapleton","Colm");

            ModifPrenom(p, "Titi");
            ModifPrenom(p, "AJC");
            ModifNom(p, "Toto");

            Console.WriteLine(p.Prenom);
            Console.WriteLine(p.Nom);


            int testOld = 5;
            int testNew = 10;
            ModifInt(ref testOld, testNew);
            Console.WriteLine(testOld);

            Console.ReadLine();
            */


            //**Exemple Polymorphisme**
            //Personne p = new Student("Colm", "Stapleton", 2019, "C#");
            //Console.WriteLine(p.SePresenter());



            Student s = new Student("Colm", "Stapleton", 2019, "C#");
            //Console.WriteLine(s.SePresenter());

            Student s2 = (Student)s.Clone();
            Console.WriteLine(s2.SePresenter());

            Console.ReadLine();

        }
        /*
        public static void ModifNom(Personne pers, String nom)
        {
            pers.Nom = nom;
        }
        public static void ModifPrenom(Personne pers, String prenom)
        {
            pers.Prenom = prenom;
        }
        public static void ModifInt(ref int oldValue, int newValue)
        {
            oldValue = newValue;
        }
        */
    }
}

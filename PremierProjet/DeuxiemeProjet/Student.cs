﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeuxiemeProjet
{
    public class Student : Personne, ICloneable, IStudent
    {
        private Student student;

        public int Annee { get; set; }
        public String Specialite { get; set; }

        public Student(String nom, String prenom, int annee, String specialite) : base (nom,prenom)
        {
            this.Annee = annee;
            this.Specialite = specialite;
        }

        public Student()
        {

        }
        //Ctor pour le clonage
        public Student(Student st) : base (Nom: st.Nom, Prenom: st.Prenom)
        {
            Annee = st.Annee;
            Specialite = st.Specialite;
        }

        public override String SePresenter()
        {

            String sP1 = base.SePresenter();
            String sePresenter = sP1+" "+ " Ma spécialité est : "+this.Specialite+". Je suis en année "+this.Annee +".";
            return sePresenter;
        }

        public object Clone()
        {
            return new Student(this);
        }

        public void AfficherData()
        {
            throw new NotImplementedException();
        }
    }
}

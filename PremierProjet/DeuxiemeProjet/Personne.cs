﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeuxiemeProjet
{
    public class Personne
    {
        private String prenom;
        protected String Nom { get; set; }
        protected String Prenom {
            get
            {
                return prenom;
            }
            set
            {
                if (value.Contains("AJC") || value.Equals("Formation"))
                {
                    Console.WriteLine("Impossible de changer le Prenom en AJC ou Formation");
                }
                else
                {
                    prenom = value;
                }
            }
        }

        public Personne(String Nom, String Prenom)
        {
            this.Nom = Nom;
            this.Prenom = Prenom;
        }

        public Personne()
        {

        }

        public virtual String SePresenter()
        {
            return ("Je suis "+this.Nom+" " + this.Prenom+".");
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremierProjet
{
    public class ClasseGenerique<T>
    {
        private int taille;
        private int index;
        public T[] tableau;

        public int Taille { get => taille; set => taille = value; }
        public int Index { get => index; set => index = value; }

        public ClasseGenerique(int t, int index, params T[] listeParams)
        {
            this.Taille = t;
            this.Index = index;
            this.tableau = new T[taille];

            for (int i = index; i < taille; i++)
            {
                tableau[i] = listeParams[i];
            }
        }

        public void Parcourir ()
        {
            for (int i = 0; i < tableau.Length; i++)
            {
                Console.WriteLine(tableau[i]);
            }
        }


    }
}

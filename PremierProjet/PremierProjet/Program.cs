﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremierProjet
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = 10;
            int num2 = 20;
            int num3 = 30;

            Console.WriteLine("Num 1 : {0}, Num 2 : {1}", num1, num2);

            Permut(ref num1, ref num2);

            Console.WriteLine("Num 1 : {0}, Num 2 : {1}", num1, num2);

            ClasseGenerique<int> cg = new ClasseGenerique<int>(3, 0, num1, num2, num3);

            cg.Parcourir();

            Console.ReadLine();
        }

        static void Permut<T> (ref T obj1, ref T obj2)
        {
            T temp = obj1;
            obj1 = obj2;
            obj2 = temp;
        }
    }
}

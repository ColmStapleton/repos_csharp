﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetTP
{
    class Program
    {
        static void Main(string[] args)
        {

            Worker w = new Worker("1A","Bob","Billy",new DateTime(1978,6,20), new DateTime(2012,10,8));
            Cadre c = new Cadre("2B","Jean","Dupont", new DateTime(1968, 3, 21), 1);
            Cadre c1 = new Cadre("3B", "Dupont", "Pierre", new DateTime(1968, 3, 21), 2);
            Cadre c2 = new Cadre("4B", "Durand", "Dupont", new DateTime(1958, 9, 21), 3);
            Patron p = new Patron("3C","Stapleton","Colm", new DateTime(1988, 10, 17));
            Patron p2 = new Patron("4C", "Schmitt", "Bernard", new DateTime(1955, 1, 18));

            w.ToString();
            c.ToString();
            c1.ToString();
            c2.ToString();
            p.ToString();
            p2.ToString();

            Console.ReadLine();
        }
    }
}

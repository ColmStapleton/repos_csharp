﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetTP
{
    public abstract class Employee
    {

        private String matricule;
        private String lastName;
        private String firstName;
        private DateTime dob;

        public String Matricule { get => matricule; set => matricule = value; }
        public String LastName { get => lastName; set => lastName = value; }
        public String FirstName { get => firstName; set => firstName = value; }
        public DateTime Dob { get => dob; set => dob = value; }

        public Employee()
        {
        }

        public Employee(String mat, String last, String first, DateTime date)
        {
            this.Matricule = mat;
            this.LastName = last;
            this.FirstName = first;
            this.Dob = date;
        }

        public override String ToString()
        {
            return String.Format("{0} {1} né en {2} a le matricule {3}",this.FirstName,this.LastName,this.Dob.Year,this.Matricule);
        }

        protected abstract int GetSalaire();
    }
}

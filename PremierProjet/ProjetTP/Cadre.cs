﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetTP
{
    public class Cadre : Employee
    {
        private int indice;
        public int Indice
        {
            get => indice;
            set
            {
                if (value >= 1 && value <= 4)
                {
                    indice = value;
                }
                else
                {
                    throw new Exception("Indice invalide. Il doit être entre 1 et 4");
                }
            }
        }

        public Cadre()
        {
        }

        public Cadre(string mat, string last, string first, DateTime date, int ind ) : base(mat, last, first, date)
        {
            this.Matricule = mat;
            this.LastName = last;
            this.FirstName = first;
            this.Dob = date;
            this.Indice = ind;
        }

        protected override int GetSalaire()
        {
            int salaire;
            switch(this.Indice)
            {
                case (1):
                    salaire = 13000;
                    break;
                case (2):
                    salaire = 15000;
                    break;
                case (3):
                    salaire = 17000;
                    break;
                case (4):
                    salaire = 20000;
                    break;
                default:
                    salaire = 0;
                    break;
            }

            return salaire;
        }

        public override String ToString()
        {
            string val = "";
            Console.WriteLine("Le cadre {0} Il a un salaire mensuel de {1} et un indice de {2}", base.ToString(), GetSalaire(), this.Indice);
            return val;
        }
    }
}

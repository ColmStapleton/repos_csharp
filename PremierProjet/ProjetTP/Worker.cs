﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetTP
{
   
    public class Worker : Employee
    {
        private static int valSmic = 2500;
        private DateTime dateEntree;

        public int ValSmic { get => valSmic; }
        public DateTime DateEntree { get => dateEntree; }
        public Worker()
        {
        }

        public Worker(string mat, string last, string first, DateTime date, DateTime entree) : base(mat, last, first, date)
        {
            this.Matricule = mat;
            this.LastName = last;
            this.FirstName = first;
            this.Dob = date;
            this.dateEntree = entree;
        }

        protected override int GetSalaire()
        {
            int salaire = ValSmic + (Anciennete()*100);
            if (salaire > ValSmic * 2)
            {
                salaire = ValSmic * 2;
            }
            return salaire;
        }

        private int Anciennete()
        {
            int anciennete = DateTime.Now.Year- this.DateEntree.Year;
            return anciennete;
        }

        public override String ToString()
        {
            String val = "";
            Console.WriteLine("L'ouvrier {0} Il a une ancienneté de {1} années et un salaire mensuel de {2}", base.ToString(), Anciennete(), GetSalaire());
            return val;
        }
    }
}

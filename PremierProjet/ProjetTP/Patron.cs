﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetTP
{
    public class Patron : Employee
    {
        private int chiffreAffaires = 1000000;
        private double pcCA = 0.05;

        public int ChiffreAffaires { get => chiffreAffaires; }
        public double PcCA { get => pcCA; }

        public Patron()
        {
        }

        public Patron(string mat, string last, string first, DateTime date) : base(mat, last, first, date)
        {
            this.Matricule = mat;
            this.LastName = last;
            this.FirstName = first;
            this.Dob = date;
        }

        protected override int GetSalaire()
        {
            double salaire = ChiffreAffaires * PcCA;
            return (int)salaire;
        }

        public override string ToString()
        {
            string val = "";
            Console.WriteLine("Le patron {0} Il a un salaire annuel de {1}", base.ToString(), GetSalaire());
            return val;
        }
    }
}
